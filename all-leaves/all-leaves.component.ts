import { Component, OnInit,Input, Output, EventEmitter } from '@angular/core';
import { ILeaves } from '../leaves.types';

@Component({
  selector: 'app-all-leaves',
  templateUrl: './all-leaves.component.html',
  styleUrls: ['./all-leaves.component.scss']
})
export class AllLeavesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  @Input() allLeaves:ILeaves[]=[];
  @Output() selectLeave=new EventEmitter<string>();
  data:any;

  selectbutton(leaveType:string,leaveReason:string){
    this.data=[leaveType,leaveReason];
    this.selectLeave.emit(this.data);
    
    
  }

  


}
