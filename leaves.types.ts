export interface ILeaves{
    leaveReason:string;
    leaveDate:string;
    isAllSelected?:boolean;
    isFloaterSelected?:boolean;
    isPlannedSelected?:boolean;

}