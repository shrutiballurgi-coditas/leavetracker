import { Component, OnInit } from '@angular/core';
import {ILeaves} from './leaves.types';
import { DataService} from './data.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'angularAssignment';
  leavesList:ILeaves[]=[];

  get allLeaves(){
    return this.leavesList.filter((leave) => leave.isAllSelected ===true);
  }

  get floaterLeaves(){
    return this.leavesList.filter((leave) => leave.isFloaterSelected === true);
  }

  get plannedLeaves(){
    return this.leavesList.filter((leave) => leave.isPlannedSelected === true);
  }


  constructor(private dataService:DataService){

  }
  ngOnInit(): void {
    this.dataService.getLeaves().subscribe({next:(result)=>{console.log(result);
      this.leavesList=result;
    }});
  }

  toggle(data:any){
    for (let leave of this.allLeaves){
    if(data[0]==='planned' && leave.leaveReason===data[1]){
      leave.isAllSelected=false;
      leave.isPlannedSelected=!leave.isPlannedSelected;
      leave.isFloaterSelected=false;
    }
    else if(data[0]==='floater' && leave.leaveReason===data[1]){
      leave.isAllSelected=false;
      leave.isPlannedSelected=false;
      leave.isFloaterSelected=!leave.isFloaterSelected;
    }
    else if(data[0]==='all' && leave.leaveReason===data[1]){
      leave.isAllSelected=!leave.isAllSelected;
      leave.isPlannedSelected=false;
      leave.isFloaterSelected=false;
    }
  }
  }
}
