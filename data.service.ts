import { Injectable } from '@angular/core';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private dataService:HttpService) { }
  
  getLeaves(){
    return this.dataService.get();
  }
}
