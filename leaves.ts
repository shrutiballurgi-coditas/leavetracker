import { AllLeavesComponent } from "./all-leaves/all-leaves.component";
import {ILeaves} from "./leaves.types";

export const leaves:ILeaves[]=[
    { 
    leaveReason:"diwali",
    leaveDate:"1/11/2020",
    isAllSelected:false,
    isFloaterSelected:false,
    isPlannedSelected:false,
},
{ leaveReason:"holi",
    leaveDate:"1/3/2020",
    isAllSelected:false,
    isFloaterSelected:false,
    isPlannedSelected:false,
},
{ leaveReason:"dussehra",
    leaveDate:"1/8/2020",
    isAllSelected:false,
    isFloaterSelected:false,
    isPlannedSelected:false,
},
{ leaveReason:"christmas",
leaveDate:"1/12/2020",
isAllSelected:false,
isFloaterSelected:false,
isPlannedSelected:false,
},
{ leaveReason:"Eid",
leaveDate:"1/4/2020",
isAllSelected:false,
isFloaterSelected:false,
isPlannedSelected:false,
}
]