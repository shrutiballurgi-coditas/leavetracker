import { Injectable } from '@angular/core';
import { leaves } from './leaves';
import { delay, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor() {}

  get(){
    return of(leaves).pipe(delay(2000));
  }
}